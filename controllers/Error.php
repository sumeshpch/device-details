<?php

/**
 *  Test Error Controller
 *
 * @package Test
 * @author  sumesh <sumeshpch@gmail.com>
 * @version SVN: $Id$
 */

namespace device\controllers;

/**
 * Error page
 *
 * @author  sumesh <sumeshp@device.co.in>
 */
class Error extends \device\core\Controller {

    /**
     * Page not found
     *
     * @return  NULL
     */
    public function showNotFound() {
        $this->getResponse()->setStatus(404);
        $this->view = APP_PATH . '/views/error/404.tpl.php';
        $this->render();
    }

    public function showError() {
        $this->view = APP_PATH . '/views/error/error.tpl.php';
        $this->render();
    }

    public function showRedirect() {
        $this->view = APP_PATH . '/views/error/redirect.tpl.php';
        $this->render();
    }

}
