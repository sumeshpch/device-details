<?php

/**
 *  Wedding Registration class
 *
 * @package Test
 * @author  sumesh <sumeshpch@gmail.com>
 * @version SVN: $Id$
 */

namespace device\controllers;

use device\core\MMException;

require_once APP_PATH . '/models/Admanage.php';

/**
 *
 * @author  sumesh <sumeshp@device.co.in>
 */
class Admanage extends \device\core\RestController {

    /**
     * Show contacts form
     *
     * @author  sumesh <sumeshpch@gmail.com>
     * @return  NULL
     */
    public function getAds($args) {
        $type = $args[1];
        $id = $args[2];

        try {
            $objAd = new \device\models\Admanage(array('request' => $this->getRequest()));
            $ads = $objAd->getAds($type, $id);
            if (is_array($ads)) {
                $this->response($ads, 200);
            } else {
                $resdata = array('status' => "FAIL");
                $this->response($resdata, 400);
            }
        } catch (MMException $e) {
            $this->handleException($e);
        }
    }

    public function updateAd() {
        try {
            $objAd = new \device\models\Admanage(array('request' => $this->getRequest()));
            $ads = $objAd->updateAd($type, $id);
            if (is_array($ads)) {
                $this->response($ads, 200);
            } else {
                $resdata = array('status' => "FAIL");
                $this->response($resdata, 400);
            }
        } catch (MMException $e) {
            $this->handleException($e);
        }
    }

    public function createAd() {
        $data = $this->getRequest()->getData();

        try {
            $objAd = new \device\models\Admanage(array('request' => $this->getRequest()));
            $ads = $objAd->createAd();
            if (is_array($ads)) {
                $this->response($ads, 200);
            } else {
                $resdata = array('status' => "FAIL");
                $this->response($resdata, 400);
            }
        } catch (MMException $e) {
            $this->handleException($e);
        }
    }

    public function saveDevice() {
        $data = $this->getRequest()->getData();

        try {
            $objAd = new \device\models\Admanage(array('request' => $this->getRequest()));
            $ads = $objAd->saveDevice($type, $id);
            if (is_array($ads)) {
                $this->response($ads, 200);
            } else {
                $resdata = array('status' => "FAIL");
                $this->response($resdata, 400);
            }
        } catch (MMException $e) {
            $this->handleException($e);
        }
    }

}
