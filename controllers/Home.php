<?php

/**
 *  Test Home class
 *
 * @package Test
 * @author  sumesh <sumeshpch@gmail.com>
 * @version SVN: $Id$
 */

namespace device\controllers;

/**
 * Home page
 *
 * @author  sumesh <sumeshpch@gmail.com>
 */
class Home extends \device\core\Controller {

    /**
     * Show Home page
     *
     * @author  sumesh <sumeshpch@gmail.com>
     * @return  NULL
     */
    public function deviceDetect() {
        $this->view = APP_PATH . '/views/home/index.tpl.php';
        $this->render();
    }

}
