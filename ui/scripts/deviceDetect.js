var scriptloaded = false;

$(document).ready(function () {
    $("#findDeviceDetails").click(function () {
        $("#deviseDetails").hide();
        $("#deviseDetailsLoading").show();

        if (!scriptloaded) {
            $.getScript("https://theapicompany.com/deviceAPI.js", function () {
                scriptloaded = true;
                loadDetails();
            });
        } else {
            loadDetails();
        }
    });
});

loadDetails = function () {
    $("#deviceBrand span").text(deviceAPI.deviceBrand);
    $("#browserName span").text(deviceAPI.browserName);
    $("#deviceName span").text(deviceAPI.deviceName);
    $("#deviceType span").text(deviceAPI.deviceType);
    $("#osName span").text(deviceAPI.osName);
    $("#touchScreen span").text(deviceAPI.touchScreen);

    $("#deviseDetails").show();
    $("#deviseDetailsLoading").hide();

    $.ajax({
        url: APP_URL + "/api/deviceDetails/save",
        method: "POST",
        data: {type: deviceAPI.deviceType, os: deviceAPI.osName},
        cache: false,
        processData: false,
        async: true,
        success: function (result) {
            var json = JSON.parse(result);

            if (json["status"] == "success") {
                alert("Device data added successfully");
            } else {
                alert("Some error occured. Please try later!")
            }
        }
    });
}