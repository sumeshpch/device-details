<?php

use \device\core\Util;

require(APP_PATH . '/views/includes/header.tpl.php');

$this->jsArray = [
    'deviceDetect.js'
];
?>

<h1>Find device details</h1>
<button id="findDeviceDetails">Click to find</button>
<div id="deviseDetailsLoading" class="devideDetailsLoading">Loading. Please wait...</div>
<div id="deviseDetails" class="devideDetails">
    <div id="deviceBrand" class="classification">
        Device brand: <span></span>
    </div>
    <div id="browserName" class="classification">
        Browser name: <span></span>
    </div>
    <div id="deviceName" class="classification">
        Device name: <span></span>
    </div>
    <div id="deviceType" class="classification">
        Device type: <span></span>
    </div>
    <div id="osName" class="classification">
        OS name: <span></span>
    </div>
    <div id="touchScreen" class="classification">
        Touchscreen: <span></span>
    </div>
</div>
<script type="text/javascript">
var APP_URL = "<?= APP_URL ?>";
</script>
<?php require(APP_PATH . '/views/includes/footer.tpl.php'); ?>