<?php
include(APP_PATH . '/views/includes/header.tpl.php');
?>
<div class="col-xs-12 error">
    <div class="col-xs-12 col-sm-10 center text-center">
        <h1>Oops!</h1>
        <p class="notFound-text">Couldn't load data for the page, please try again in a few minutes.<br />We are sorry for the inconvenience.</p>
        <p class="notFound-text">Try going back to the <a href="<?php echo APP_URL ?>/" target="_parent">home page</a> and look for something else.</p>
    </div>
</div>
<?php
include(APP_PATH . '/views/includes/footer.tpl.php');
?>
