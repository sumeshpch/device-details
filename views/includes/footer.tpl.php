<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<?php
if (isset($this->jsArray) && count($this->jsArray) > 0) {
    foreach ($this->jsArray as $jsFile) {
        echo "<script type=\"text/javascript\" src=\"" . \device\core\Util::getAssetUrl($jsFile, 'js') . "\"></script>" . chr(13);
    }
}
?>

</body>
</html>