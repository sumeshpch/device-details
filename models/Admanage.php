<?php

/**
 * User ad class 
 * @author  sumesh <sumeshpch@gmail.com>
 * @version SVN: $Id$
 */

namespace device\models;

use device\core\Container;

/**
 * Register model
 *
 * @author  sumesh <sumeshpch@gmail.com>
 */
class Admanage extends \device\core\Model {

    /**
     * validate insert data
     *
     * @return : boolean 
     */
    private function _isValidCreate() {
        $data = $this->getRequest()->getData();
        if (!trim($data['title'])) {
            throw new MMException("Please enter title.", 400);
        } else {
            if (!preg_match("/^[a-zA-Z _.]+$/", $data['title'])) {
                throw new MMException("Title should not contain invalid characters and numbers", 400);
            }
        }
        if (!trim($data['text'])) {
            throw new MMException('Please enter some text');
        }
        if (!trim($data['sponsoredBy'])) {
            throw new MMException('Please enter sponsor name');
        }
        return true;
    }

    /**
     * Register
     *
     * @return : boolean 
     */
    public function saveContactForm() {
        $data = $this->getRequest()->getData();
        $validation = $this->_isValidContact();
        if ($validation) {
            try {
                $this->_sendContactMail($data);
                return true;
            } catch (\Exception $e) {
                //$this->setErrorMessage('Some error occured. Please try later!');
                $this->setErrorMessage($e->getMessage());
                $logger = Container::getService('logger');
                $logger->error('M4M_EXCPT: ' . __FILE__ . '(' . __LINE__ . '): ' . $e->getMessage());
            }
        }
        return false;
    }

    public function getAds($type, $id) {
        if (empty($id)) {
            throw new MMException("Please provide a valid Id.", 400);
        }

        $dbReader = Container::getService('dbReader');

        switch ($type) {
            case 'ad':
                $query = "SELECT `title`, `text`, `image`, `sponsoredBy`, `trackingUrl` FROM `ads` WHERE `id` = '" . $id . "'";
                break;
            case 'campaign':
                $query = "SELECT `title`, `text`, `image`, `sponsoredBy`, `trackingUrl` FROM `ads` WHERE `campaignId` = '" . $id . "'";
                break;
            case 'advertiser':
                $query = "SELECT `title`, `text`, `image`, `sponsoredBy`, `trackingUrl` FROM `ads` "
                        . "JOIN `campaign` ON (`campaign`.`id` = `ads`.`campaignId`) WHERE `campaign`.`advertiserId` = '" . $id . "'";
                break;
            default:
                throw new MMException("Please provide a valid type.", 400);
                break;
        }

        $ret = array();
        $rs = $dbReader->query($query);

        if ($rs->getCount() > 0) {
            while ($row = $rs->fetch()) {
                $ret[] = $row;
            }
        }
        $rs->close();

        return $ret;
    }

    public function updateAd() {
        $data = $this->getRequest()->getData();

        if (empty($data['id'])) {
            throw new MMException("Please provide a valid Id.", 400);
        }

        $dbWriter = Container::getService('dbWriter');

        $updates = array();
        $itemCount = 0;

        if (!empty($data['title'])) {
            $updates[] = "`title` = '" . $data['title'] . "'";
            $itemCount++;
        }
        if (!empty($data['text'])) {
            $updates[] = "`text` = '" . $data['text'] . "'";
            $itemCount++;
        }
        if (!empty($data['image'])) {
            $updates[] = "`image` = '" . $data['image'] . "'";
            $itemCount++;
        }
        if (!empty($data['sponsoredBy'])) {
            $updates[] = "`sponsoredBy` = '" . $data['sponsoredBy'] . "'";
            $itemCount++;
        }

        if ($itemCount == 0) {
            throw new MMException("Please select atleast one field.", 400);
        }

        $updateQuery = "UPDATE `ads` SET " . implode(",", $updates) . " WHERE `id` = '" . $data['id'] . "'";

        if ($dbWriter->execute($updateQuery)) {
            $return['status'] = "SUCCESS";
        }
        $dbWriter->close();

        return $return;
    }

    public function createAd() {
        $data = $this->getRequest()->getData();

        $this->_isValidCreate();

        $dbWriter = Container::getService('dbWriter');

        $inserts = array();
        if (!empty($data['title'])) {
            $inserts[] = "'" . $data['title'] . "'";
        }
        if (!empty($data['text'])) {
            $inserts[] = "'" . $data['text'] . "'";
        }
        if (!empty($data['image'])) {
            $inserts[] = "'" . $data['image'] . "'";
        }
        if (!empty($data['sponsoredBy'])) {
            $inserts[] = "'" . $data['sponsoredBy'] . "'";
        }

        $insertQuery = "INSERT INTO `ads`(`title`,`text`,`image`,`sponsoredBy`) VALUES (" . implode(",", $inserts) . ")";

        if ($dbWriter->execute($insertQuery)) {
            $return['status'] = "SUCCESS";
        }
        $dbWriter->close();

        return $return;
    }

    public function saveDevice() {
        $data = $this->getRequest()->getData();

        $dbWriter = Container::getService('dbWriter');

        $ip = $_SERVER['REMOTE_ADDR'];
        $inserts = array();
        if (!empty($data['type'])) {
            $inserts[] = "'" . $data['type'] . "'";
        }
        if (!empty($data['os'])) {
            $inserts[] = "'" . $data['os'] . "'";
        }
        if (!empty($ip)) {
            $inserts[] = "'" . $ip . "'";
        }

        $insertQuery = "INSERT INTO `devices`(`type`,`os`,`ip`) VALUES (" . implode(",", $inserts) . ") ON DUPLICATE KEY UPDATE `ip` = '" . $ip . "'";

        if ($dbWriter->execute($insertQuery)) {
            $return['status'] = "SUCCESS";
        }
        $dbWriter->close();

        return $return;
    }

}
