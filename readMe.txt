# Challenge for Web Developers

I am happy to answer all the parts that i was required to do.

I have added the file namely "challengeWebDeveloper_Answers.txt" in the folder "/theTest" which contains the first part(MySQL) answer and the API endpoint definitions.

The code implementation is been added to this repo which is done with PHP + Javascript and MySQL. The Database table structure is attached in the file "dbQueries.sql"

Requires Apache server with .htaccess enabled and Virtual host. Please add the virtual host URL in "/conf/settings.php" file as a constant variable in "APP_URL" 

Also add the database configurations in "/conf/settings.php" as constant variables.