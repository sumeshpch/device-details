<?php

/**
 * Test settings file
 *
 * @package Test
 * @author  sumesh <sumeshpch@gmail.com>
 * @version SVN: $Id$
 *
 */
//=================== APPLICATION  SETTINGS ========================
define('APP_PATH', dirname(__DIR__));
define('ENV_MODE', "prod");

define('APP_URL', '');
define('THEME_URL', APP_URL . '/ui');
define('THEME_STYLE_URL', THEME_URL . '/styles');
define('THEME_SCRIPT_URL', THEME_URL . '/scripts');

//error log
define('ERROR_LOG', '/var/log/apache2/test.error.log');

//db config
define('DB_NAME', '');
define('DB_HOST', '');
define('DB_PORT', '');
define('DB_USER', '');
define('DB_PASSWORD', '');

/**
 * stop
 * @param mixed $var
 * @return void
 */
function stop($var) {
    pre($var);
    exit;
}

/**
 * print_r in pre tag
 * @param $var
 */
function pre($var) {
    echo "<pre>";
    print_r($var);
    echo "</pre>";
}
