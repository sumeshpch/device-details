<?php

/**
 * This  file contains all services configuration in the application
 *
 * @author  sumesh <sumeshpch@gmail.com>
 * @version SVN: $Id$
 */
use device\core\Container;

include(APP_PATH . '/core/DB.php');
require APP_PATH . '/conf/masters.php';     // Including the master values
require APP_PATH . '/core/RestController.php';

$dbWriteParams = array('dbName' => DB_NAME,
    'dbHost' => DB_HOST,
    'dbPort' => DB_PORT,
    'dbUser' => DB_USER,
    'dbPassword' => DB_PASSWORD,
    'mode' => 'master');
$dbReadParams = array('dbName' => DB_NAME,
    'dbHost' => DB_HOST,
    'dbPort' => DB_PORT,
    'dbUser' => DB_USER,
    'dbPassword' => DB_PASSWORD,
    'mode' => 'slave');

Container::setSharedService('logger', 'device\\core\\Logger', 'Logger.php');
Container::setSharedService('dbWriter', 'device\\core\\DB', 'DB.php', $dbWriteParams);
Container::setSharedService('dbReader', 'device\\core\\DB', 'DB.php', $dbReadParams);
