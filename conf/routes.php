<?php

/**
 * This  file contains all routes in the application
 * @package Test
 * @author  sumesh <sumeshpch@gmail.com>
 * @version SVN: $Id$
 */
use device\core\Router;

//device detect page url
Router::add('/deviceDetails/', array('GET' => array('class' => 'Home', 'action' => 'deviceDetect')));

//api urls
Router::add('/api/ad/get/([0-9A-Za-z\-\_]+)*/([0-9]+)*', array('GET' => array('class' => 'Admanage', 'action' => 'getAds')));
Router::add('/api/ad/update', array('PUT' => array('class' => 'Admanage', 'action' => 'updateAd')));
Router::add('/api/ad/create', array('POST' => array('class' => 'Admanage', 'action' => 'createAd')));
Router::add('/api/deviceDetails/save', array('POST' => array('class' => 'Admanage', 'action' => 'saveDevice')));
