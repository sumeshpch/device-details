# Challenge for Web Developers



## MySQL

Assume the following scenario: There are ads, each belonging to exactly one campaign. A campaign can have multiple ads. Each campaign belongs to exactly one advertiser. An advertiser can have multiple campaigns. Only the following ad properties are relevant for the task: _title, text, image, sponsoredBy, trackingUrl_. 
Assuming those tables are already present in a MySQL server produce the mysql queries to fulfill the following tasks:

* showing all campaigns of advertiser #100 that have more than 50 ads
*ANS* - SELECT `campaign`.`name`, count(`ads`.`id`) AS adCount FROM `campaign` JOIN `ads` ON (`ads`.`campaignId` = `campaign`.`id`) WHERE `campaign`.`advertiserId` = '100' GROUP BY `campaign`.`id` HAVING adCount >= 50;

* showing all campaigns that do not have any ads
*ANS* - SELECT `campaign`.`name` FROM `campaign` LEFT JOIN `ads` ON (`ads`.`campaignId` = `campaign`.`id`) WHERE `ads`.`id` IS NULL;



## API

Assume the data structure from the mysql task. Please produce rest-ful API calls for the following actions. Do not implement anything here, just specify _URL, HTTP method_ and the _request body_ for each action.

* selecting a specific ad
*ANS* - /api/ad/get/ad/{ad id} - GET

* selecting all ads of a specific campaign
*ANS* - /api/ad/get/campaign/{campaign id} - GET

* selecting all ads of a specific advertiser
*ANS* - /api/ad/get/advertiser/{advertiser id} - GET

* creating an ad
*ANS* - /api/ad/create - POST - {title: "some title", text: "some text", image: "base64 image", sponsoredBy: "sponsor name"}

* modifying a specific ad
*ANS* - /api/ad/update - PUT - {title: "optional title", text: "optional text", image: "optional base64 image", sponsoredBy: "optional sponsor name"}

# The above mentioned URLs are defined in "/conf/routes.php" file with the corresponding method and the Controller/Action used to satisfy the request.