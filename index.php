<?php

/**
 * This  file is the gateway to the front end. It is responsible for intercepting requests
 * instantiating appropriate classes and calling the execute function.
 *
 * @package Test
 * @author  sumesh <sumeshpch@gmail.com>
 * @version SVN: $Id$
 *
 */
//ini_set('display_errors',1);

require 'conf/settings.php';
require APP_PATH . '/core/Container.php';
require APP_PATH . '/core/Router.php';
require APP_PATH . '/conf/routes.php';     // Including the routes

require APP_PATH . '/core/Controller.php';
require APP_PATH . '/core/Model.php';
require APP_PATH . '/core/Request.php';
require APP_PATH . '/core/Response.php';

require APP_PATH . '/conf/services.php';   // Including the configs
require APP_PATH . '/core/ErrorHandler.php';
require APP_PATH . '/core/MMException.php';
require APP_PATH . '/core/InitHandler.php';

require APP_PATH . '/core/Session.php';
require APP_PATH . '/core/Util.php';
require APP_PATH . '/core/Masters.php';

device\core\ErrorHandler::init();
device\core\InitHandler::init();

$requestObj = new device\core\Request(); //GET the request object
$responseObj = new device\core\Response(); //GET the response object
try {
    $params = device\core\Router::parse();
    $class = $params['class'];
    $method = $params['action'];
    $matches = $params['matches'];
    include APP_PATH . "/controllers/$class.php";
    $namespacedClass = 'device\\controllers\\' . $class;
    if (class_exists($namespacedClass)) {
        $obj = new $namespacedClass(array('request' => $requestObj, 'response' => $responseObj));
        if (method_exists($obj, $method)) {
            $obj->setDefaultView($class, $method);
            $obj->$method($matches);
        } else {
            throw new BadMethodCallException("Method, $method, not supported.");
        }
    } else {
        throw new BadMethodCallException("Class, $class, not found.");
    }
} catch (BadMethodCallException $exception) {
    include APP_PATH . "/controllers/Error.php";
    $error = new device\controllers\Error(array('request' => $requestObj, 'response' => $responseObj));
    $error->showNotFound();
} catch (UnexpectedValueException $exception) {
    include APP_PATH . "/controllers/Error.php";
    $error = new device\controllers\Error(array('request' => $requestObj, 'response' => $responseObj));
    $error->showNotFound();
}
