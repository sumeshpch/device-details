<?php

namespace device\core;

use device\core\Controller;
use device\core\Container;
use device\core\ErrorHandler as hander;

/**
 * M4marrayapi Rest Controller
 * A fully RESTful server implementation for m4marry , one config file and one controller.
 * @author        	Sunil N jose
 * @version 		1.0
 */
class RestController extends Controller {

    /**
     * List of allowed HTTP methods
     *
     * @var array
     */
    protected $allowed_http_methods = array('get', 'delete', 'post', 'put');

    /**
     * General request data and information.
     * Stores accept, language, body, headers, etc.
     *
     * @var object
     */
    public $request = NULL;

    /**
     * What is gonna happen in output?
     *
     * @var object
     */
    protected $response = NULL;

    /**
     * If the request is allowed based on the API key provided.
     *
     * @var boolean
     */
    protected $_allow = TRUE;

    /**
     * Format  of response type
     *
     * @var String
     */
    public $format = "json";

    /**
     * Data to send response
     *
     * @var String
     */
    public $data = "";

    /**
     * Call back string for jsonp;
     *
     * @var string
     */
    protected $_callback = "callback";

    /**
     * compress is needed or not
     *
     * @var boolen
     */
    protected $compress_output = false;

    /**
     * User id is  of access token
     *
     * @var string
     */
    protected $_user_id = 0;

    /**
     * _debug used for debug
     *
     * @var boolean
     */
    protected $_debug = false;

    /**
     * api used for debug
     *
     * @var array
     */
    protected $_accessInfo = array();

    /**
     * List all supported methods, the first will be the default format
     *
     * @var array
     */
    protected $_supported_formats = array(
        'xml' => 'application/xml; charset=UTF-8',
        'json' => 'application/json; charset=UTF-8',
        'jsonp' => 'application/javascript; charset=UTF-8',
    );

    /**
     * zlib  header compression is needed or not
     *
     * @var Boolean
     */
    protected $_zlib_oc = FALSE;

    /**
     * Constructor function
     *
     */
    public function __construct(array $config = array()) {
        set_exception_handler(array($this, 'handleException'));


        if (!isset($config['request']) || !isset($config['response'])) {
            throw new \InvalidArgumentException("Request or Reponse object not set");
        }
        $this->setRequest($config['request']);
        $this->setResponse($config['response']);
        $this->initialize();
    }

    /**
     * Constructor function ,initialize and validate the access token before giving access to controller
     * 
     */
    public function initialize() {
        $this->_zlib_oc = @ini_get('zlib.output_compression');
        $this->request = $this->getRequest()->getFilteredData();
        $response = array();
        if (isset($this->request['format'])) {
            $format = strtolower($this->request['format']);
            if ($this->request['debug'] === "true") {
                $this->_debug = true;
            }
            if (!$this->_supported_formats[$format]) {

                $bodyConfig = $this->getApiConfig('rest');
                $msg = $bodyConfig['msg_01'];
                $this->response($msg, $msg['code']);
            } else {
                $this->format = $format;
            }
        }

        $this->_accessInfo = $response;
    }

    /**
     * Send the data to output buffer,Takes pure data and optionally a status code, then creates the response.
     * @param array $data
     * @param null|int $http_code
     */
    public function response($data = array(), $http_code = null) {

        $responseObj = $this->getResponse();
        // If data is empty and not code provide, error and bail
        if (empty($data) && $http_code === null) {
            $http_code = 404;
            // create the output variable here in the case of $this->response(array());
            $output = NULL;

            // Otherwise (if no data but 200 provided) or some data, carry on camping!
        } else {

            // Is compression requested?
            if ($this->compress_output === TRUE && $this->_zlib_oc == FALSE) {
                if (extension_loaded('zlib')) {
                    if (isset($_SERVER['HTTP_ACCEPT_ENCODING']) AND strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') !== FALSE) {
                        ob_start('ob_gzhandler');
                    }
                }
            }
            is_numeric($http_code) OR $http_code = 200;
            // If the format method exists, call and return the output in that format
            if (method_exists($this, 'format_' . $this->format)) {
                $header = 'Content-Type: ' . $this->_supported_formats[$this->format];
                $responseObj->setHeader($header);
                $output = $this->{'format_' . $this->format}($data);
            } else {

                $output = "";
            }
        }


        $responseObj->setStatus($http_code);

        // If zlib.output_compression is enabled it will compress the output,
        // but it will not modify the content-length header to compensate for
        // the reduction, causing the browser to hang waiting for more data.
        // We'll just skip content-length in those cases.
        if (!$this->_zlib_oc and ! $this->compress_output) {
            $header2 = 'Content-Length: ' . strlen($output);
            $responseObj->setHeader($header2);
        }


        $responseObj->setContent($output);

        $content = $responseObj->getContent();

        $responseObj->sendResponse();
    }

    /**
     * Send the data to output buffer,Takes pure data and optionally a status code, then creates the response.
     * @param array $data
     * @param null|int $http_code
     */
    public function customResponse($data = array(), $http_code = null) {

        $responseObj = $this->getResponse();
        // If data is empty and not code provide, error and bail
        if (empty($data) && $http_code === null) {
            $http_code = 404;
            // create the output variable here in the case of $this->response(array());
            $output = NULL;

            // Otherwise (if no data but 200 provided) or some data, carry on camping!
        } else {

            // Is compression requested?
            if ($this->compress_output === TRUE && $this->_zlib_oc == FALSE) {
                if (extension_loaded('zlib')) {
                    if (isset($_SERVER['HTTP_ACCEPT_ENCODING']) AND strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') !== FALSE) {
                        ob_start('ob_gzhandler');
                    }
                }
            }
            is_numeric($http_code) OR $http_code = 200;
            // If the format method exists, call and return the output in that format
            $header = 'Content-Type: ' . $this->_supported_formats[$this->format];
            $responseObj->setHeader($header);
        }
        $output = $data;

        $responseObj->setStatus($http_code);

        // If zlib.output_compression is enabled it will compress the output,
        // but it will not modify the content-length header to compensate for
        // the reduction, causing the browser to hang waiting for more data.
        // We'll just skip content-length in those cases.
        if (!$this->_zlib_oc and ! $this->compress_output) {
            $header2 = 'Content-Length: ' . strlen($output);
            $responseObj->setHeader($header2);
        }



        $responseObj->setContent($output);

        $responseObj->sendResponse();
    }

    /**
     * Check if the client's ip is in the 'rest_ip_whitelist' config
     */
    protected function _check_whitelist_auth() {
        $whitelist = explode(',', config_item('rest_ip_whitelist'));

        array_push($whitelist, '127.0.0.1', '0.0.0.0');

        foreach ($whitelist AS &$ip) {
            $ip = trim($ip);
        }

        if (!in_array($this->input->ip_address(), $whitelist)) {
            $this->response(array('code' => 401, 'error' => 'not_authorized', "message" => "The request is not authorized"), 401);
        }
    }

    /**
     * Encode as JSONP
     *
     * @param array $data The input data.
     * @return string The JSONP data string (loadable from Javascript).
     */
    protected function format_jsonp($data = array()) {
        return $this->_callback . '(' . $this->format_json($data) . ')';
    }

    /**
     * Check the header for a ajax call
     *  @return  the header value
     */
    public function is_ajax_request() {
        return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest');
    }

    /**
     * Format XML for output
     *
     * @param mixed $data
     * @param SimpleXMLElement $structure
     * @param string $basenode
     * @return string The data as XML string.
     */
    public function format_xml($data = null, $structure = null, $basenode = 'xml') {
        if ($data === null) {
            return false;
        }

        // Turn off compatibility mode as SimpleXML throws behaves badly if it 
        // is enabled.
        if (ini_get('zend.ze1_compatibility_mode') == 1) {
            ini_set('zend.ze1_compatibility_mode', 0);
        }

        if ($structure === null) {
            $structure = simplexml_load_string("<?xml version='1.0' encoding='utf-8'?><$basenode />");
        }

        // Force it to be something useful
        if (!is_array($data) AND ! is_object($data)) {
            $data = (array) $data;
        }

        foreach ($data as $key => $value) {
            // No numeric keys in our xml please!
            if (is_numeric($key)) {
                // Make string key...           
                //$key = ($this->singular($basenode) != $basenode) ? $this->singular($basenode) : 'item';
                $key = 'item';
            }

            // Replace anything not alpha numeric
            $key = preg_replace('/[^a-z_\-0-9]/i', '', $key);

            // If there is another array found recrusively call this function
            if (is_array($value) || is_object($value)) {
                $node = $structure->addChild($key);

                // Recrusive call.
                $this->format_xml($value, $node, $key);
            } else {
                // Add single node.
                $value = htmlspecialchars(html_entity_decode($value, ENT_QUOTES, 'UTF-8'), ENT_QUOTES, "UTF-8");

                $structure->addChild($key, $value);
            }
        }

        return $structure->asXML();
    }

    /**
     * Encode to JSON string
     * @param $body array ,the array to json encode
     * @return string The JSON encoded data.
     */
    public function format_json($body = array()) {
        $jsonBody = json_encode($body);
        if ($this->_debug) {
            return $this->prettyJson($jsonBody);
        }
        return $jsonBody;
    }

    /**
     * Override the ErrorHandler ,handleException methode.
     * @param Exception object
     * @return the output buffer
     */
    public function handleException($e) {

        if (get_class($e) === "api\core\MMException") {
            $exception = $e->returnError();

            $error['error']['code'] = $exception['error']['code'];
            $error['error']['type'] = "invalid_request";
            $error['error']['message'] = $exception['error']['info'];
            $this->response($error, $error['error']['code']);
            unset($e);
        } else {
            hander::handleException($e);
        }
    }

    /**
     * prettyJson a flat JSON string to make it more human-readable for debugging purpouses.its call only when debug param is true
     * @param string $json The original JSON string to process.
     * @return string Indented version of the original JSON string.
     */
    private function prettyJson($json) {

        $result = '';
        $pos = 0;
        $strLen = strlen($json);
        $indentStr = '  ';
        $newLine = "\n";
        $prevChar = '';
        $outOfQuotes = true;

        for ($i = 0; $i <= $strLen; $i++) {

            // Grab the next character in the string.
            $char = substr($json, $i, 1);

            // Are we inside a quoted string?
            if ($char == '"' && $prevChar != '\\') {
                $outOfQuotes = !$outOfQuotes;

                // If this character is the end of an element, 
                // output a new line and indent the next line.
            } else if (($char == '}' || $char == ']') && $outOfQuotes) {
                $result .= $newLine;
                $pos --;
                for ($j = 0; $j < $pos; $j++) {
                    $result .= $indentStr;
                }
            }

            // Add the character to the result string.
            $result .= $char;

            // If the last character was the beginning of an element, 
            // output a new line and indent the next line.

            if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
                $result .= $newLine;
                if ($char == '{' || $char == '[') {
                    $pos ++;
                }

                for ($j = 0; $j < $pos; $j++) {
                    $result .= $indentStr;
                }
            }

            $prevChar = $char;
        }

        return $result;
    }

    /**
     * Get token from request, if the auth param is come along as 
     * AUTHORIZATION header the authentication is conside JWS else JWE
     * 
     * @param None
     * 
     * @return String
     */
    public function checkUserSession() {
        //Get request params from globals
        $requests = $this->getRequestFromGlobals();
        $token = $this->getAccessTokenValue(
                $requests['query'], $requests['request'], $requests['server'], $requests['headers']
        );



        $this->_user_token = $token;
        if ($this->getUserUsingAccessToken($token)) {

            Container::set('userId', $this->_user_id);
            Container::set('userInfo', $this->_user_info);

            return $this->_user_info;
        } else {
            throw new MMException(
            'Unbale to authenticate the user.', 401, "not_authorized"
            );
        }
    }

    /**
     * Retrieve global params
     * 
     * @param None
     * 
     * @return Array
     */
    public function getRequestFromGlobals() {
        //Get params
        $query = $_GET;

        //Post params
        $request = $_POST;

        //Server params
        $server = $_SERVER;

        //Headers form server
        $headers = $this->getHeadersFromServer($server);

        //Get content type
        $contentType = isset($headers['CONTENT_TYPE']) ?
                $headers['CONTENT_TYPE'] : '';

        //Get request method
        $requestMethod = isset($headers['REQUEST_METHOD']) ?
                $headers['REQUEST_METHOD'] : 'GET';

        if (0 === strpos($contentType, 'application/x-www-form-urlencoded') //Content type application/x-www-form-urlencoded
                && in_array(strtoupper($requestMethod), array('PUT', 'DELETE'))) {
            parse_str($this->getRequest()->getContent(), $request);
        } elseif (0 === strpos($contentType, 'application/json') //Content type application/json
                && in_array(strtoupper($requestMethod), array('POST', 'PUT', 'DELETE'))) {
            $request = json_decode($this->getRequest()->getContent(), true);
        }

        return array(
            'request' => $request,
            'query' => $query,
            'server' => $server,
            'headers' => $headers
        );
    }

    /**
     * Retrieve headers from server
     * 
     * @param Array $server
     * 
     * @return Array
     */
    private function getHeadersFromServer($server) {
        $headers = array();

        foreach ($server as $key => $value) {
            if (0 === strpos($key, 'HTTP_')) {
                $headers[substr($key, 5)] = $value;
            }
            // CONTENT_* are not prefixed with HTTP_
            elseif (in_array($key, array('CONTENT_LENGTH', 'CONTENT_MD5', 'CONTENT_TYPE'))) {
                $headers[$key] = $value;
            }
        }

        if (isset($server['PHP_AUTH_USER'])) {
            $headers['PHP_AUTH_USER'] = $server['PHP_AUTH_USER'];
            $headers['PHP_AUTH_PW'] = isset($server['PHP_AUTH_PW']) ? $server['PHP_AUTH_PW'] : '';
        } else {
            /*
             * php-cgi under Apache does not pass HTTP Basic user/pass to PHP by default
             * For this workaround to work, add this line to your .htaccess file:
             * RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
             *
             * A sample .htaccess file:
             * RewriteEngine On
             * RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
             * RewriteCond %{REQUEST_FILENAME} !-f
             * RewriteRule ^(.*)$ app.php [QSA,L]
             */

            $authorizationHeader = null;
            if (isset($server['HTTP_AUTHORIZATION'])) {
                $authorizationHeader = $server['HTTP_AUTHORIZATION'];
            } elseif (isset($server['REDIRECT_HTTP_AUTHORIZATION'])) {
                $authorizationHeader = $server['REDIRECT_HTTP_AUTHORIZATION'];
            } elseif (function_exists('apache_request_headers')) {
                $requestHeaders = apache_request_headers();

                // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
                $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));

                if (isset($requestHeaders['Authorization'])) {
                    $authorizationHeader = trim($requestHeaders['Authorization']);
                }
            }

            if (null !== $authorizationHeader) {
                $headers['AUTHORIZATION'] = $authorizationHeader;
                // Decode AUTHORIZATION header into PHP_AUTH_USER and PHP_AUTH_PW when authorization header is basic
                if (0 === stripos($authorizationHeader, 'basic')) {
                    $exploded = explode(':', base64_decode(substr($authorizationHeader, 6)));
                    if (count($exploded) == 2) {
                        list($headers['PHP_AUTH_USER'], $headers['PHP_AUTH_PW']) = $exploded;
                    }
                }
            }
        }

        // PHP_AUTH_USER/PHP_AUTH_PW
        if (isset($headers['PHP_AUTH_USER'])) {
            $headers['AUTHORIZATION'] = 'Basic ' . base64_encode($headers['PHP_AUTH_USER'] . ':' . $headers['PHP_AUTH_PW']);
        }
        return $headers;
    }

    /**
     * Get access token value
     * 
     * @param type $query
     * @param type $request
     * @param type $server
     * @param type $headers
     * 
     * @return String
     * 
     * @throws \Exception
     */
    private function getAccessTokenValue($query, $request, $server, $headers) {

        $headers = !empty($headers['AUTHORIZATION']) ? $headers['AUTHORIZATION'] : null;
        // Check that exactly one method was used
        $methodsUsed = !empty($headers) +
                !empty($query[SSO_ACCESS_TOKEN_PARAM_NAME]) +
                !empty($request[SSO_ACCESS_TOKEN_PARAM_NAME]);

        if ($methodsUsed > 1) {
            throw new MMException(
            'Only one method may be used to authenticate at a time (Auth header, GET or POST)', 400, "not_authorized"
            );
        }

        if ($methodsUsed == 0) {
            throw new MMException(
            'Authenticate  header is  missing (Auth header, GET or POST)', 401, "not_authorized"
            );
        }

        // HEADER: Get the access token from the header
        if (!empty($headers)) {
            if (!preg_match('/' . SSO_ACCESS_TOKEN_PARAM_NAME . '\s(\S+)/', $headers, $matches)) {
                throw new MMException(
                'Malformed auth header', 400, "not_authorized"
                );
            }
            return $matches[1];
        }

        if (!empty($request[SSO_ACCESS_TOKEN_PARAM_NAME])) {
            // POST: Get the token from POST data
            if (strtolower($server['REQUEST_METHOD']) != 'post') {
                throw new MMException(
                'When putting the token in the body, the method must be POST', 400, "not_authorized"
                );
            }

            $contentType = $server['CONTENT_TYPE'];
            if (false !== $pos = strpos($contentType, ';')) {
                $contentType = substr($contentType, 0, $pos);
            }

            if ($contentType !== null && $contentType != 'application/x-www-form-urlencoded') {
                // IETF specifies content-type. NB: Not all webservers populate this _SERVER variable
                // @see http://tools.ietf.org/html/rfc6750#section-2.2
                throw new MMException(
                'The content type for POST requests must be "application/x-www-form-urlencoded"', 400, "not_authorized"
                );
            }

            return $request[SSO_ACCESS_TOKEN_PARAM_NAME];
        }

        // GET method
        return $query[SSO_ACCESS_TOKEN_PARAM_NAME];
    }

    /**
     * Get the value from simple db
     * 
     * @param None
     * 
     * @return String
     */
    function getUserUsingAccessToken($token) {
        $this->checkToken($token);
        $tokenAuthentication = Container::getService('tokenAuthentication');
        try {
            if ($tokenAuthentication->verifyJWStoken($token)) {
                $userData = $tokenAuthentication->getPayload($token);
                //print_r($userData);exit;
                $this->_user_info = $userData;
                $this->_user_id = $userData['sub'];
                $exp = $userData['exp'];
                $expiry = $exp + TOKEN_EXPIRY_TIME; //(24*60*60);
                //$expiry = $exp+(60);
                $this->_user_token = $token;
                if (time() > $expiry) {
                    throw new MMException(
                    'Token expired.', 400, "token_expire"
                    );
                }
                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {

            throw new MMException(
            'Unbale to authenticate the user,expection in token validation', 400, "not_authorized"
            );
        }
    }

    /**
     * Get the value from simple db
     * 
     * @param None
     * 
     * @return String
     */
    function checkToken($token = "") {
        $tokenValue = str_replace("Bearer", "", $token);
        $tokenValue = trim($tokenValue);
        if (empty($tokenValue)) {

            throw new MMException(
            'Authenticate  header is  missing (Auth header, GET or POST)', 400, "not_authorized"
            );
        }
    }

}
