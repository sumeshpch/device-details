<?php

/**
 * Controller base class. All controller classes would extend this.
 *
 * @package Test
 * @author  sumesh <sumeshpch@gmail.com>
 * @version SVN: $Id$
 */

namespace device\core;

/**
 * Parent Controller class. Inherited by all controllers
 *
 * @author  sumesh <sumeshpch@gmail.com>
 */
class Controller {

    /**
     * The Request object
     * @var \device\core\Request
     */
    private $_request;

    /**
     * The Response object
     * @var \device\core\Response
     */
    private $_response;

    /**
     * view template
     */
    protected $view;

    /*
     *  array to store variables to be used in view
     */
    protected $data = array();

    /**
     * Class constructor. Initializes class and
     * assigns the request and response object
     *
     * @param array $config config array
     * @throws \InvalidArgumentException
     */
    function __construct(array $config = array()) {
        if (!isset($config['request']) || !isset($config['response'])) {
            throw new \InvalidArgumentException("Request or Reponse object not set");
        }

        $this->setRequest($config['request']);
        $this->setResponse($config['response']);
    }

    /**
     * Sets the request object
     *
     * @param Request $request The request object
     *
     * @return void
     */
    public function setRequest(Request $request) {
        $this->_request = $request;
    }

    /**
     * Returns the request object
     *
     * @return Request object
     */
    public function getRequest() {
        return $this->_request;
    }

    /**
     * Sets the response object
     *
     * @param Response $response The response object
     *
     * @return null
     */
    public function setResponse(Response $response) {
        $this->_response = $response;
    }

    /**
     * Gets the response object
     *
     * @return Response response object
     */
    public function getResponse() {
        return $this->_response;
    }

    /**
     * Sets the View to be rendered
     *
     * @param string $folder folder containing the view
     * @param string $view   filename of the view
     *
     * @return void
     */
    public function setDefaultView($folder, $view) {
        $this->view = APP_PATH . '/views/' . strtolower($folder) . '/' . $view . '.tpl.php';
    }

    /**
     * Sets the value for the key
     *
     * @param string $key   key
     * @param mixed $value value
     *
     * @return void
     */
    protected function setData($key, $value) {
        $this->data[$key] = $value;
    }

    /**
     * Sets the template
     *
     * @param string $view value
     *
     * @return void
     */
    protected function setTemplate($view) {
        $this->view = APP_PATH . $view;
    }

    /**
     * Sets variables
     *
     * @param string $vars variables
     *
     * @return void | false
     */
    protected function setVars($vars) {
        if (!is_array($vars)) {
            return false;
        }
        $this->data = array_merge($this->data, $vars);
    }

    /**
     * Renders the view
     * @throws \Exception
     * @return void
     */
    protected function render() {
        try {
            extract($this->data);
            ob_start();
            include $this->view;
            $content = ob_get_contents();
            ob_end_clean();
            $this->getResponse()->setContent($content);
            $this->getResponse()->sendResponse();
        } catch (Exception $e) {
            throw new \Exception('View, ' . $this->view . ' not found.');
        }
    }

    /**
     * Returns the view
     * @throws \Exception
     * @return string
     */
    protected function getView() {
        try {
            ob_start();
            extract($this->data);
            include $this->view;
            $view = ob_get_contents();
            ob_end_clean();
            return $view;
        } catch (Exception $e) {
            throw new \Exception('View, ' . $view . ' not found.');
        }
    }

    /**
     * Redirects
     *
     * @param string $url url
     *
     * @return void
     */
    public static function redirect($url) {
        //bug, Header may not contain more than a single header, new line detected, below line may solve it
        // $url = ereg_replace( ' +', '%20', $url);
        header("Location: $url");
        exit;
    }

    public function getRedirectURL() {
        $url = $_SERVER['REQUEST_URI'];
        return $url;
    }

    public function sendAPIResponse($myInbox) {
        $responseObj = $this->getResponse();
        $responseObj->setTTL(0);
        $responseObj->setContentType('application/json');
        $responseObj->setContent(json_encode($myInbox));
        $responseObj->sendResponse();
    }

}
