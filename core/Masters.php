<?php

/**
 * Masters class
 *
 * @author  sumesh <sumeshpch@gmail.com>
 * @version SVN: $Id$
 */

namespace device\core;

use device\core\Container;

class Masters {

    /**
     * To get data
     *
     * @param String $index Key of the master config
     * @param Int    $id    id
     *
     * @return  Array
     *
     */
    private static function _getData($index, $id = "") {
        $masterConfig = Container::get('MASTER_CONFIG');
        if ($id) {
            return isset($masterConfig[$index][$id]) ? $masterConfig[$index][$id] : array();
        } else {
            return isset($masterConfig[$index]) ? $masterConfig[$index] : array();
        }
    }

}
