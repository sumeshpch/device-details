<?php

/**
 *
 * Util class
 * @author  sumesh <sumeshpch@gmail.com>
 * @version SVN: $Id$
 */

namespace device\core;

/**
 * Common util class
 *
 * @author  sumesh <sumeshpch@gmail.com>
 */
class Util {

    /**
     * Fetches the asset url
     *
     * @param string $name name
     * @param string $type type
     * ca
     * @return  string
     */
    public static function getAssetUrl($name, $type) {
        $prefix = ($type == "css") ? THEME_STYLE_URL : THEME_SCRIPT_URL;
        $url = $prefix . '/' . $name;
        return $url;
    }

    /**
     * Displays the html options
     *
     * @param array $rsArr   rs array
     * @param array $select  select
     * @param array $ucwords convert case
     * @param array $return  return
     *
     * @return String HTML
     */
    public static function getHTMLOptions($rsArr, $select = '', $ucwords = false, $return = false) {
        $str = '';
        if (!$rsArr) {
            return false;
        }
        $selIsArray = is_array($select);

        foreach ($rsArr as $key => $value) {
            $selected = '';
            if ($selIsArray) {
                if (in_array($key, $select)) {
                    $selected = " selected";
                }
            } elseif ($select !== '' && $key == $select) {
                $selected = " selected";
            }

            if ($ucwords) {
                $str .= "<option value=\"$key\" $selected>" . ucwords($value) . "</option>\n";
            } else {
                $str .= "<option value=\"$key\" $selected>$value</option>\n";
            }
        }
        if ($return) {
            return $str;
        } else {
            echo $str;
        }
    }

    /**
     * Displays the html radio buttons
     *
     * @param string $name           name
     * @param array  $rsArr          rs array
     * @param array  $select         select
     * @param array  $container      container
     * @param array  $containerClass Container class
     * @param array  $inputClass     Input class
     * @param array  $ucwords        convert case
     *
     * @return String HTML
     */
    public static function getHTMLRadioBoxes($name, $rsArr, $select = '', $container = '', $containerClass = '', $inputClass = '', $ucwords = false) {

        if (!$rsArr) {
            return false;
        }

        $class = ($containerClass) ? "class=\"$containerClass\"" : "";
        $inputClass = ($inputClass) ? "class=\"$inputClass\"" : "";

        foreach ($rsArr as $key => $value) {
            $selected = '';
            if ($select && $key == $select) {
                $selected = " checked=\"checked\"";
            }
            if ($ucwords) {
                $value = ucwords($value);
            }

            if ($container) {
                echo "<$container $class>";
            }
            echo "<input $inputClass type=\"radio\" name=\"$name\" value=\"$key\" $selected/>$value";
            if ($container) {
                echo "</$container>";
            }
        }
    }

    /**
     * Displays the html checkboxes
     *
     * @param string $name           name
     * @param array  $rsArr          rs array
     * @param array  $select         select
     * @param array  $container      container
     * @param array  $containerClass Container class
     * @param array  $inputClass     Input class
     * @param array  $ucwords        convert case
     *
     * @return String HTML
     */
    public static function getHTMLCheckBoxes($name, $rsArr, $select = '', $container = '', $containerClass = '', $inputClass = '', $ucwords = false, $parentContainer = '', $parentContainerClass = '') {

        if (!$rsArr) {
            return false;
        }

        $class = ($containerClass) ? "class=\"$containerClass\"" : "";
        $inputClass = ($inputClass) ? "class=\"$inputClass\"" : "";
        $parentContainerClass = ($parentContainerClass) ? "class=\"$parentContainerClass\"" : "";

        foreach ($rsArr as $key => $value) {
            $selected = '';
            if (is_array($select)) {
                if (in_array($key, $select)) {
                    $selected = "checked=checked";
                }
            } elseif ($select && $key == $select) {
                $selected = "checked=checked";
            }
            if ($ucwords) {
                $value = ucwords($value);
            }

            if ($parentContainer) {
                echo "<$parentContainer $parentContainerClass>";
            }
            if ($container) {
                echo "<$container $class>";
            }
            if ($name == "") {
                echo "<input $inputClass type=\"checkbox\" value=\"$key\" $selected/>$value";
            } else {
                echo "<input $inputClass type=\"checkbox\" name=\"{$name}[]\" value=\"$key\" $selected/>$value";
            }
            if ($container) {
                echo "</$container>";
            }
            if ($parentContainer) {
                echo "</$parentContainer>";
            }
        }
    }

    public static function getErrorMessage($errors = array(), $field = '') {
        if (isset($errors[$field]) && $errors[$field] != 'Blank') {
            echo "<span class=\"error\">$errors[$field]</span>";
        }
        /* if (isset($errors[$field])) {
          if($errors[$field] == 'Blank') {
          $error = 'This field is required';
          } else {
          $error = $errors[$field];
          }

          echo "<span class=\"error\">$error</span>";
          } */
        return '';
    }

    public static function getErrorSymbol($errors = array(), $field = '') {
        if (isset($errors[$field])) {
            echo "<b class=\"errorImg\" title=\"This field is required\"></b>";
        }
        return '';
    }

    public static function getErrorClass($errors = array(), $field = '') {
        if (isset($errors[$field])) {
            echo 'has-error';
        }
        return '';
    }

    public static function getCSVHTML($valArray, $keyArray) {
        $str = array();
        $keys = explode(",", $keyArray);
        foreach ($keys as $key) {
            $str[] = $valArray[$key];
        }
        return implode(", ", $str);
    }

    /**
     * stop
     *
     * @param int $var Mixed var
     *
     * @return  void
     *
     */
    public static function stop($var) {
        self::pre($var);
        exit;
    }

    /**
     * pre
     *
     * @param int $var Mixed var
     *
     * @return  ''
     *
     */
    public static function pre($var) {
        echo "<pre>";
        print_r($var);
        echo "</pre>";
    }

    /**
     * Get mail template
     *
     * @return  string
     *
     */
    public static function getMailTemplate($pathToTemplate, $data = array()) {
        ob_start();

        /*
          if(is_array($data)){
          if(!empty($data)){
          extract($data);
          }
          } */

        include(APP_PATH . $pathToTemplate);
        $returnString = ob_get_contents();
        ob_end_clean();
        return $returnString;
    }

    /**
     * add dates
     *
     *  @param date $date date from which number to be addded
     *
     *  @param int $days-days to add
     *
     * @return  date
     *
     */
    public static function dateAdd($date, $days) {
        $addedDate = strtotime('+' . $days . ' day', strtotime($date));
        $addedDate = date('Y-m-d', $addedDate);
        return $addedDate;
    }

    /**
     * subtract dates
     *
     *  @param date $date date from which number to be subtracted
     *
     *  @param int $days-days to subtract
     *
     * @return  date
     *
     */
    public static function dateSub($date, $days) {
        $addedDate = strtotime('-' . $days . ' day', strtotime($date));
        $addedDate = date('Y-m-d', $addedDate);
        return $addedDate;
    }

    /**
     * Show Message (error/success..)
     *
     * @param string $msg message,$msgType type of message
     *
     * @return  string
     */
    public static function showMessage($msg = '', $msgType = '') {
        if (trim($msg) == '') {
            $display = 'style="display:none"';
            $msg = '&nbsp;';
        } else {
            $display = 'style="display:block"';
        }
        if (trim($msgType) == 'success') {
            $msgClass = 'alert-success';
            $msgValue = 'Success';
        } else {
            $msgClass = 'alert-danger';
            $msgValue = 'Error';
        }
        echo '<div id="msgBox" class="alert ' . $msgClass . '" ' . $display . '> <button class="close" type="button">×</button><b>' . $msg . '</b></div>';
    }

    /**
     * Add zero padding to array index
     *
     * @param array() $array input array
     *
     * @return array() formatted array
     */
    public static function getFomatedArray($array) {
        $processed = array();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $value = self::getFomatedArray($value);
            }
            $key = str_pad($key, strlen(count($array)), '0', STR_PAD_LEFT);
            $processed[$key] = $value;
        }
        return $processed;
    }

    /**
     * convert UTC to IST
     *
     * @return  Array
     *
     */
    public static function getGMTTime($utcTime = '') {
        if (!$utcTime) {
            $utcTime = date('Y-m-d H:i:s');
        }
        $date = new \DateTime($utcTime);
        $date->setTimezone(new \DateTimeZone('Asia/Calcutta'));
        return $date->format('Y-m-d H:i:s');
    }

    public static function fieldname($name, $salt) {
        return sha1($name . $salt . SALT_KEY);
    }

    public static function encodeInvalidXmlCharcaters($str) {
        //to handle encoded and nonencoded "&"
        $str = preg_replace("/&(?!amp;)(?!lt;)(?!gt;)(?!quot;)(?!#039;)/", "&amp;", $str);

        $search = array('<', '>', '"', '\'', '&#039;');
        $replace = array('&lt;', '&gt;', '&quot;', '&apos;', '&apos;');
        $str = str_replace($search, $replace, $str);

        return $str;
    }

    /**
     * Validate Int
     *
     * @author sony <sony@device.co.in>
     * @return array
     */
    public static function isValidInt($value) {
        if (is_array($value)) {
            foreach ($value as $val) {
                if (!preg_match('/^[0-9]+$/', $val)) {
                    return false;
                }
            }
        } else {
            if (!preg_match('/^[0-9]+$/', $value)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Validate false|true
     *
     * @author sony <sony@device.co.in>
     * @return array
     */
    public static function isValidBoolString($value) {
        if (!preg_match('/^false|true$/i', $value)) {
            return false;
        }
        return true;
    }

    /**
     * Validate false|true
     *
     * @author sony <sony@device.co.in>
     * @return array
     */
    public static function isValidDate($date, $format = 'Y-m-d') {
        switch ($format) {
            case 'd-m-Y' :
                if (!preg_match('/\d{2}-\d{2}-\d{4}/', $date)) {
                    return false;
                }
                break;
            default :
                if (!preg_match('/\d{4}-\d{2}-\d{2}/', $date)) {
                    return false;
                }
                break;
        }
        return true;
    }

    public static function stripNonNumeric($string) {
        return preg_replace("/[^0-9,.]/", "", $string);
    }

    /**
     * get multi selected array in order
     *
     * * @return  array
     *
     */
    public static function getMultiSelectedInOrder($selectedArray, $masterArray) {
        $newArray = array();
        foreach ($selectedArray as $selectedVal) {
            if (array_key_exists($selectedVal, $masterArray)) {
                $newArray[$selectedVal] = $masterArray[$selectedVal];
            }
        }
        return $newArray;
    }

    public function removeNullValuesFromArrayCallback($value) {
        if ($value == null) {
            return false;
        }
        return true;
    }

    /**
     * Used to remove null values from array
     *
     * * @return  array
     *
     */
    public static function removeNullValuesFromArray($unfilteredArray, $allowedKeys) {
        $filterArray = array_filter($unfilteredArray, array(new Util, removeNullValuesFromArrayCallback));
        $resultArrayKeys = array_intersect(array_keys($filterArray), $allowedKeys);
        $resultArrayQuery = '';
        foreach ($resultArrayKeys as $key => $value) {
            $resultArray[$value] = $filterArray[$value];
            $resultArrayQuery .= "$value=$filterArray[$value]&";
        }
        $resultArrayQuery = trim($resultArrayQuery, '&');
        return $resultArrayQuery;
    }

    /**
     * get the masters array as objects to be used in javascript(in correct order)
     *
     * @param array() $array input array
     *
     * @return array() formatted array
     */
    public static function getMastersForJS($masterArray) {
        if (count($masterArray) < 1) {
            return array();
        }

        $processed = array();
        foreach ($masterArray as $key => $value) {
            $processed[] = array($key => $value);
        }
        return $processed;
    }

    /**
     * get the group details and mapping for multiselect grouping
     *
     * @param array() $array input array
     *
     * @return array() formatted array
     */
    public static function getMultiselectGroups($masterArray) {
        if (count($masterArray) < 1) {
            return array();
        }

        $groups = array();
        $groupMap = array();
        foreach ($masterArray as $key => $value) {
            $groupId = $key;
            $groups[] = array($groupId => $value['name']);

            $groupEntries = $value['ids'];

            if (is_array($groupEntries)) {
                foreach ($groupEntries AS $groupEntry) {
                    $groupMap[$groupEntry] = $groupId;
                }
            }
        }

        return array('groups' => $groups, 'groupsMap' => $groupMap);
    }

    /**
     * check if the value selected is a valid master entry
     *
     * @param string $values input value
     * @param array() $array input array
     *
     * @return boolean
     */
    public static function isValidMasterEntry($value, $master) {
        if (!$value) {
            return true;
        }

        $eachValue = explode(',', $value);
        foreach ($eachValue as $valueToCheck) {
            if (!array_key_exists($valueToCheck, $master)) {
                return false;
            }
        }
        return true;
    }

    public static function underVerifSymbol($underVerificationaArray = array(), $field = '') {
        echo (in_array($field, $underVerificationaArray)) ? '<a href="javascript:void(0)" class="pending-tooltip icon-form-help fl-pending-apr" title="This text will be approved and reflected in your account shortly.">&nbsp;Ver</a>' : '';
    }

    public static function mandFieldsRejectedSymbol($rejectedArray = array(), $field = '', $underVerificationArray = array()) {
        echo (array_key_exists($field, $rejectedArray) && !in_array($field, $underVerificationArray)) ? '<a href="javascript:void(0)" class="icon-form-help icon-form-rejected fl-rej" title="&ldquo;' . $rejectedArray[$field] . '&rdquo; ( Rejected by m4marry.com )">&nbsp;Rej</a>' : '';
    }

    public static function encryptPassword($message) {
        $nonceSize = openssl_cipher_iv_length(CYPHER_METHOD);
        $nonce = openssl_random_pseudo_bytes($nonceSize);
        $ciphertext = openssl_encrypt($message, CYPHER_METHOD, CYPHER_KEY, OPENSSL_RAW_DATA, $nonce);
        return base64_encode($nonce . $ciphertext);
    }

    public static function decryptPassword($encryptedMessage) {
        $encryptedMessage = base64_decode($encryptedMessage, true);
        $nonceSize = openssl_cipher_iv_length(CYPHER_METHOD);
        $nonce = mb_substr($encryptedMessage, 0, $nonceSize, '8bit');
        $cipherText = mb_substr($encryptedMessage, $nonceSize, null, '8bit');
        $message = openssl_decrypt($cipherText, CYPHER_METHOD, CYPHER_KEY, OPENSSL_RAW_DATA, $nonce);
        return $message;
    }

    public static function formatAmount($amount) {
        if (!$amount) {
            return '';
        }
        return number_format($amount, 2);
    }

}
