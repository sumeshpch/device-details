<?php

/**
 *
 * Initilization classs
 *
 * The `InitHandler` class allows all the initilizations.
 *
 * @author  sumesh <sumeshpch@gmail.com>
 * @version SVN: $Id$
 */

namespace device\core;

/**
 * InitHandler
 *
 * @author dhanesh <dhanesh@device.co.in>
 */
Class InitHandler {

    /**
     * init do the initilizations of the variables used in settings.php
     *
     * @return void
     */
    public static function init() {
        self::setTimezone();
        //self::setAppUrl();
        self::setAjaxLoader();
    }

    /**
     * To get the user id from the session
     *
     * @return  userId
     */
    public static function getUserId() {
        $userId = Session::read('M4MARRY');

        return $userId;
    }

    /**
     * To set the URL for logged in and not logged in users 
     *
     * @return  void
     */
    public static function setAppUrl() {
        //
    }

    /**
     * To set time zone 
     *
     * @return  void
     */
    public static function setTimezone() {
        date_default_timezone_set('UTC'); //Setting tiem zone
    }

    /**
     * To set AJAX loader path
     *
     * @return  void
     */
    public static function setAjaxLoader() {
        //================= ajax loader ======================================
        define('AJAX_LOADER', THEME_URL . '/images/ajxLoader.gif');
    }

}
