<?php

/**
 *
 * ErrorHandler classs
 *
 * The `ErrorHandler` class allows PHP errors and exceptions to be handled in a uniform way.
 *
 * @package Test
 * @author  sumesh <sumeshpch@gmail.com>
 * @version SVN: $Id$
 */

namespace device\core;

/**
 * ErrorHandler
 *
 * @author  sumesh <sumeshpch@gmail.com>
 */
class ErrorHandler {

    /**
     * Custom  error handler function to log errors
     *
     * @param int $errNumber  Error number
     * @param str $errMessage Error message
     * @param str $file       File
     * @param int $line       Line number
     * @param str $context    Context
     *
     * @return void
     */
    public static function handleError($errNumber, $errMessage, $file, $line, $context) {
        $error = "$file:$line  $errMessage";
        $logger = Container::getService('logger');
        switch ($errNumber) {
            case E_NOTICE:
            case E_USER_NOTICE:
                //NOTICE
                //$/logger->notice($error);
                return true;
                break;

            case E_WARNING:
            case E_CORE_WARNING;
            case E_USER_WARNING;
                //WARN
                $logger->warn($error);
                return true;
                break;

            case E_ERROR:
            case E_USER_ERROR:
            case E_CORE_ERROR:
            case E_RECOVERABLE_ERROR:
                //ERR
                $logger->error($error);
                break;

            default:
                $logger->debug($error);
                break;
            //INFO
        }
        //Send error response
        self::sendError();
        exit;
    }

    /**
     * Custom  exception handling function to log exceptions
     *
     * @param Exception $exception Exception
     *
     * @return void
     */
    public static function handleException($exception) {
        $file = $exception->getFile();
        $line = $exception->getLine();
        $errMessage = $exception->getMessage();
        $trace = self::getExceptionTraceAsString($exception);

        $error = "$file:$line  $errMessage $trace";

        $logger = Container::getService('logger');
        $logger->excpt($error);

        self::sendError();
        //$this->redirect(APP_URL."/error?msg=Invalid request!");
        exit;
    }

    /**
     * Send 500 response code
     *
     * @return void
     */
    public static function sendError() {
        //TODO: send 404
        header("Location: " . APP_URL . "/error");
    }

    /**
     * init sets the error/excpetion handlers 
     *
     * @return void
     */
    public static function init() {
        set_error_handler(array("device\core\ErrorHandler", "handleError"));
        set_exception_handler(array("device\core\ErrorHandler", "handleException"));
    }

    /**
     * Returns Exception trace as string without truncation
     *
     * @return exception string
     */
    public static function getExceptionTraceAsString($exception) {
        $rtn = "";
        $count = 0;
        foreach ($exception->getTrace() as $frame) {
            $args = "";
            if (isset($frame['args'])) {
                $args = array();
                foreach ($frame['args'] as $arg) {
                    if (is_string($arg)) {
                        $args[] = "'" . $arg . "'";
                    } elseif (is_array($arg)) {
                        $args[] = "Array";
                    } elseif (is_null($arg)) {
                        $args[] = 'NULL';
                    } elseif (is_bool($arg)) {
                        $args[] = ($arg) ? "true" : "false";
                    } elseif (is_object($arg)) {
                        $args[] = get_class($arg);
                    } elseif (is_resource($arg)) {
                        $args[] = get_resource_type($arg);
                    } else {
                        $args[] = $arg;
                    }
                }
                $args = join(", ", $args);
            }
            $rtn .= sprintf("#%s %s(%s): %s(%s)\n", $count, isset($frame['file']) ? $frame['file'] : 'unknown file', isset($frame['line']) ? $frame['line'] : 'unknown line', (isset($frame['class'])) ? $frame['class'] . $frame['type'] . $frame['function'] : $frame['function'], $args);
            $count++;
        }
        return $rtn;
    }

}
