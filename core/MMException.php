<?php

/**
 *
 * Base class for MM Excpetions
 *
 * @author  sumesh <sumeshpch@gmail.com>
 */

namespace device\core;

/**
 * MMException class
 *
 * @author  sumesh <sumeshpch@gmail.com>
 */
class MMException extends \Exception {

    var $type = "";

    public function __construct($message, $code = 0, $type = "", Exception $previous = null) {
        // some code
        $this->type = $type;
        // make sure everything is assigned properly
        parent::__construct($message, $code, $previous);
    }

    /**
     * Function to return the error
     *
     * @return array error
     */
    public function returnError() {

        $ret = array(
            'error' => array(
                'code' => $this->getCode(),
                'type' => $this->type,
                'info' => $this->getMessage()
            )
        );
        return $ret;
    }

}
